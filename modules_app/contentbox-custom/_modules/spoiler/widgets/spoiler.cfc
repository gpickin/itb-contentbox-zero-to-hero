/**
* ContentBox - A Modular Content Platform
* Copyright since 2012 by Ortus Solutions, Corp
* www.ortussolutions.com/products/contentbox
* ---
* A cool basic widget that shows the next game
*/
component extends="contentbox.models.ui.BaseWidget" singleton{

	spoiler function init(){
		// Widget Properties
		setName( "Spoiler" );
		setVersion( "1.0" );
		setDescription( "A widget that shows the ending of the movie, only if you want it" );
		setAuthor( "Black and Blue" );
		setAuthorURL( "http://www.blackandbluewebapps.com" );
		setIcon( "game" );
		setCategory( "Avengers" );
		return this;
	}

	/**
	* A widget that shows the ending of the movie, only if you want it
	* @spoilertext.label Spoiler Ending
	* @spoilertext.hint Defaults to You're not worthy
	*/
	any function renderIt(
		string spoilertext="You're not worthy"
	){
        saveContent variable="rString"{
			writeOutput( renderView( view="/spoiler", args=arguments, module="spoiler" ) );
		}

		return rString;
	}

}
