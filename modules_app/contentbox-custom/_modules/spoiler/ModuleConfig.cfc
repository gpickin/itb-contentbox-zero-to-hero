/**
* ContentBox - A Modular Content Platform
* Copyright since 2012 by Ortus Solutions, Corp
* www.ortussolutions.com/products/contentbox
* ---
*/
component hint="Spoiler Module for Avenger Site"{

	// Module Properties
	this.title 				= "Spoiler";
	this.author 			= "Gavin Pickin - Black and Blue Web Apps";
	this.webURL 			= "http://www.blackandbluewebapps.com";
	this.description 		= "Spoiler Module for Avenger Site";
	this.version			= "1.0";
	// If true, looks for views in the parent first, if not found, then in the module. Else vice-versa
	this.viewParentLookup 	= true;
	// If true, looks for layouts in the parent first, if not found, then in module. Else vice-versa
	this.layoutParentLookup = true;
	// Module Entry Point
	this.entryPoint			= "spoiler";

	function configure(){

		// parent settings
		parentSettings = {

		};

		// module settings - stored in modules.name.settings
		settings = {

		};

		// Layout Settings
		layoutSettings = {
			defaultLayout = ""
		};

		// datasources
		datasources = {

		};

		// web services
		webservices = {

		};

		// SES Routes
		routes = [
			// Module Entry Point
			{pattern="/", handler="home",action="index"},
			// Convention Route
			{pattern="/:handler/:action?"}
		];

		// Custom Declared Points
		interceptorSettings = {
			customInterceptionPoints = ""
		};

		// Custom Declared Interceptors
		interceptors = [
		];

		// Binder Mappings
		// binder.map( "Alias" ).to( "#moduleMapping#.model.MyService" );

	}

	/**
	* Fired when the module is registered and activated.
	*/
	function onLoad(){
		var menuService = wirebox.getInstance( "AdminMenuService@cb" );
		menuService.addTopMenu(
			name 		= "AVENGERS",
			label 		= "<i class='fa fa-map-marker'></i> Avengers",
			permissions = ""
		)
		menuService.addSubMenu(
			topMenu = "AVENGERS",
            name 	= "my_menu",
            label 	= "<i class='fa fa-tachometer'></i> Spoliers Manager",
			href 	= "/cbadmin/module/spoiler/home/index",
			permission=""
        );
	}

	/**
	* Fired when the module is activated by ContentBox
	*/
	function onActivate(){
	}

	/**
	* Fired when the module is unregistered and unloaded
	*/
	function onUnload(){
		var menuService = wirebox.getInstance( "AdminMenuService@cb" );

		// Remove Menu Contribution only if registered
		if( menuService.getTopMenuMap().keyExists( "AVENGERS" ) ){
			menuService.removeSubMenu( name="my_menu", topMenu = "AVENGERS" );
			menuService.removeTopMenu( topMenu="AVENGERS" );
		}
		
	}

	/**
	* Fired when the module is deactivated by ContentBox
	*/
	function onDeactivate(){

	}

}